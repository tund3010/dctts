Meteor.publish('recipes', function(){
	return Recipes.find({author: this.userId});
});

Meteor.publish('contracts', function(){
	return Contracts.find();
});
Meteor.publish('customers', function(){
	return Customers.find();
});
Meteor.publish('trades', function(){
	return Trades.find();
});
Meteor.publish('masteraccounts', function(){
	return MasterAccounts.find();
});
Meteor.publish('depositwithdrawals', function(){
	return DepositWithdrawals.find();
});

Meteor.publish('SingleRecipe', function(id){
	check(id, String);
	return Recipes.find({_id: id});
});