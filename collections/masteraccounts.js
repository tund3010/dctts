MasterAccounts = new Mongo.Collection('masteraccounts');

MasterAccounts.allow({
	insert: function(userId, doc) {
		return !!userId;
	},
	update: function(userId, doc) {
		return !!userId;
	}
});

MasterAccountSchema = new SimpleSchema({
	code: {
		type: String,
		label: "Code"
	},
	desc: {
		type: String,
		label: "Description"
	},
	expiredDate: {
		type: Date,
		label: "Expired Date",
		autoValue: function() {
			return new Date()
		},
		autoform: {
			type: "hidden"
		}
	},
	enable: {
		type: Boolean,
		defaultValue: true,
		autoform: {
			type: "hidden"
		}
	},
	author: {
		type: String,
		label: "Author",
		autoValue: function () {
			return this.userId
		},
		autoform: {
			type: "hidden"
		}
	},
	createdAt: {
		type: Date,
		label: "Created At",
		autoValue: function() {
			return new Date()
		},
		autoform: {
			type: "hidden"
		}
	}
});

Meteor.methods({
	deleteMasterAccount: function(id){
		MasterAccounts.remove(id);
	}
});

MasterAccounts.attachSchema(MasterAccountSchema);