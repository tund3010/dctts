DepositWithdrawals = new Mongo.Collection('depositwithdrawals');

DepositWithdrawals.allow({
	insert: function(userId, doc) {
		return !!userId;
	},
	update: function(userId, doc) {
		return !!userId;
	}
});

DepositWithdrawalSchema = new SimpleSchema({
	dealer: {
		type: String,
		label: "Dealer",
		autoValue: function () {
			return this.userId
		},
		autoform: {
			type: "hidden"
		}
    },
    customer: {
		type: String,
		label: "Customer"
    },
	time: {
		type: Date,
		label: "Trade Time"
	},
	type: {
		type: String,
		label: "Type"
	},
	amount: {
		type: Number,
		label: "Amount"
	},
	author: {
		type: String,
		label: "Author",
		autoValue: function () {
			return this.userId
		},
		autoform: {
			type: "hidden"
		}
	},
	createdAt: {
		type: Date,
		label: "Created At",
		autoValue: function() {
			return new Date()
		},
		autoform: {
			type: "hidden"
		}
	}
});

Meteor.methods({
	deleteDepositWithdrawal: function(id){
		DepositWithdrawals.remove(id);
	}
});

DepositWithdrawals.attachSchema(DepositWithdrawalSchema);