Customers = new Mongo.Collection('customers');

Customers.allow({
	insert: function(userId, doc) {
		return !!userId;
	},
	update: function(userId, doc) {
		return !!userId;
	}
});

CustomerSchema = new SimpleSchema({
	fullName: {
		type: String,
		label: "Full Name"
	},
	customerCode: {
		type: String,
        label: "Code",
        regEx: /^[a-z0-9A-Z_]{3,15}$/,
        unique: true,
        custom() {
          if (Meteor.isClient) {
            Meteor.call("customersIsCodeAvailable", this.value, (error, result) => {
              if (!result) {
                this.validationContext.addValidationErrors([{
                  name: "customerCode",
                  type: "notUnique"
                }]);
              }
            });
          }
        }
	},
	contractNumber: {
		type: String,
        label: "Contract Number"
	},
	signedDate: {
		type: Date,
		label: "Contract Signed Date"
	},
	expiredDate: {
		type: Date,
		label: "Contract Expired Date"
	},
	idNo: {
		type: String,
		label: "Identified Number"
	},
	idIssueDate: {
		type: Date,
		label: "ID Issue Date"
	},
	idProvider: {
		type: String,
		label: "ID Provider"
	},
	address: {
		type: String,
		label: "Address"
	},
	temporaryAddress: {
		type: String,
		label: "Temporary Address"
	},
	mobile1: {
		type: String,
		label: "Mobile 1"
	},
	mobile2: {
		type: String,
		label: "Mobile 2"
	},
	email: {
		type: String,
		label: "Email"
	},
	zalo: {
		type: String,
		label: "Zalo"
	},
	facebook: {
		type: String,
		label: "Facebook"
	},
	maxTradesNum: {
		type: Number,
		label: "Maximum Trades Number"
	},
	feePerNight: {
		type: Number,
		label: "Fee Per Night"
	},
	feePerContract: {
		type: Number,
		label: "Fee Per Contract"
	},
	requireDeposit: {
		type: Number,
		label: "Require Deposit"
	},
	maxLost: {
		type: Number,
		label: "Maximum Lost"
	},
	broker: {
		type: String,
		label: "Broker"
	},
	balance: {
		type: Number,
		label: "Balance",
		autoValue: function() {
			return 0
		},
		autoform: {
			type: "hidden"
		}
	},
	enable: {
		type: Boolean,
		defaultValue: true,
		autoform: {
			type: "hidden"
		}
	},
	author: {
		type: String,
		label: "Author",
		autoValue: function () {
			return this.userId
		},
		autoform: {
			type: "hidden"
		}
	},
	createdAt: {
		type: Date,
		label: "Created At",
		autoValue: function() {
			return new Date()
		},
		autoform: {
			type: "hidden"
		}
	}
});

Meteor.methods({
	deleteCustomer: function(id){
		Customers.remove(id);
    },
    customersIsCodeAvailable: function(newCode){
        if (!!Customers.findOne({customerCode: newCode})) {
            return { error: "", result: false };
        } else {
            return { error: "", result: true };
        }
    }
});

Customers.attachSchema(CustomerSchema);