Trades = new Mongo.Collection('trades');

Trades.allow({
	insert: function(userId, doc) {
		return !!userId;
	},
	update: function(userId, doc) {
		return !!userId;
	}
});

TradeSchema = new SimpleSchema({
	dealer: {
		type: String,
		label: "Dealer",
		autoValue: function () {
			return this.userId
		},
		autoform: {
			type: "hidden"
		}
	},
    customer: {
		type: String,
		label: "Customer"
    },
	tradeTime: {
		type: Date,
		label: "Trade Time"
	},
	idMasterTrade: {
		type: String,
		label: "Id Master Trade"
	},
	type: {
		type: String,
		label: "Type"
	},
	contractCode: {
		type: String,
		label: "Contract Code"
	},
	amount: {
		type: Number,
		label: "Amount"
	},
	closedAmount: {
		type: Number,
		label: "Closed Amount"
	},
	closedPrice: {
		type: Number,
		label: "Closed Price"
	},
	locked: {
		type: Boolean,
		defaultValue: false,
		autoform: {
			type: "hidden"
		}
	},
	author: {
		type: String,
		label: "Author",
		autoValue: function () {
			return this.userId
		},
		autoform: {
			type: "hidden"
		}
	},
	createdAt: {
		type: Date,
		label: "Created At",
		autoValue: function() {
			return new Date()
		},
		autoform: {
			type: "hidden"
		}
	}
});

Meteor.methods({
	deleteTrade: function(id){
		Trades.remove(id);
	}
});

Trades.attachSchema(TradeSchema);