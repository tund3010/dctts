Template.Customers.onCreated(function(){
	var self = this;
	self.autorun(function() {
		self.subscribe('customers');
	});
});

Template.Customers.helpers({
	customers: ()=> {
		return Customers.find({});
	}
});

Template.Customers.events({
	'click .new-customer': () => {
		Session.set('newCustomer', true);
	}
});