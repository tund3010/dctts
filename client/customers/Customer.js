Template.Customer.onCreated(function(){
	this.editMode = new ReactiveVar(false);
});

Template.Customer.helpers({
	updateCustomerId: function() {
		return this._id;
	},
	editMode: function(){
		return Template.instance().editMode.get();
	}
});

Template.Customer.events({
	'click .fa-trash': function () {
		Meteor.call('deleteCustomer', this._id);
	},
	'click .fa-pencil': function (event, template) {
		template.editMode.set(!template.editMode.get());
	}
});