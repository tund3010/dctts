Template.Contracts.onCreated(function(){
	var self = this;
	self.autorun(function() {
		self.subscribe('contracts');
	});
});

Template.Contracts.helpers({
	contracts: ()=> {
		return Contracts.find({});
	}
});

Template.Contracts.events({
	'click .new-contract': () => {
		Session.set('newContract', true);
	}
});