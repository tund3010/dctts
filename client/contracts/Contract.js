Template.Contract.onCreated(function(){
	this.editMode = new ReactiveVar(false);
});

Template.Contract.helpers({
	updateContractId: function() {
		return this._id;
	},
	editMode: function(){
		return Template.instance().editMode.get();
	}
});

Template.Contract.events({
	'click .fa-trash': function () {
		Meteor.call('deleteContract', this._id);
	},
	'click .fa-pencil': function (event, template) {
		template.editMode.set(!template.editMode.get());
	}
});